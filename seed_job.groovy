@Grab('org.gitlab:java-gitlab-api:4.1.0')
import org.gitlab.api.GitlabAPI


@Grab('org.yaml:snakeyaml:1.17')
import org.yaml.snakeyaml.Yaml

import utilities.Utils
import utilities.DescriptionHelper
import utilities.MyUtilities
import java.nio.file.Paths


def token = Utils.getCredentialsById("gitlab-api-access-key")

println "Seed Job Started"

def buildPath(element, base_path=null)
{
    if(base_path != null){
        return Paths.get(base_path, element).toString()
    }else{
        return element
    }
}

def createMultibranchPipelineJob(gitlab_project, base_path=null)
{
    jenkins_job_name = buildPath(gitlab_project.name,base_path)
    multiProject = multibranchPipelineJob(jenkins_job_name){
        branchSources {
                git {
                    remote(gitlab_project.getSshUrl())
                    credentialsId('gitlab-ssh-access-key')
                }
            }
    }
    return multiProject
}

def createJobs(gitlabAPI, job_list, base_path=null)
{
    print "Generate list of jobs to ${base_path}"
    job_list.each { job_config ->
        switch ( job_config.type ) {
            case "gitlab_project":
                gitlab_project = gitlabAPI.getProject(job_config.id)
                jenkinsJob = createMultibranchPipelineJob(gitlab_project, base_path)
                break
            case "gitlab_group":

                def group = gitlabAPI.getGroup(job_config.id)
                def projects = gitlabAPI.getGroupProjects(job_config.id)
                if(job_config.folder)
                {
                    if(job_config.name)
                    {
                        jenkinsGroupName = job_config.name
                    }else {
                        jenkinsGroupName = group.name
                    }
                    jenkins_job_name = buildPath(jenkinsGroupName, base_path)
                    def jenkinsGroupFolder = folder(jenkins_job_name) {
                        displayName(jenkinsGroupName)
                        description(DescriptionHelper.buildProjectGroupDescription(group))
                    }
                    base_path = jenkins_job_name
                }

                projects.each { current_project ->
                    // Only Projects with Tag `automatic_build` will used
                    if (current_project.getTagList().contains("automatic_build")) {
                        createMultibranchPipelineJob(current_project, base_path)
                    }
                }
                break
            case "folder":
                jenkins_job_name = buildPath(job_config.name,base_path)
                def jenkinsGroupFolder = folder(job_config.name) {
                    displayName(job_config.name)
                    if(job_config.description)
                        description(job_config.description)

                }
                createJobs(gitlabAPI, job_config.jobs, jenkinsGroupFolder.name)
                break

        }
    }
}

print "Start Generate Jobs From Seed Job"

Yaml parser = new Yaml()
def seed_job_config = parser.load(("${WORKSPACE}/docs/example-config.yml" as File).text)
def gitlabAPI = GitlabAPI.connect("https://gitlab.com", token.getSecret().toString());

createJobs(gitlabAPI, seed_job_config.jobs)
