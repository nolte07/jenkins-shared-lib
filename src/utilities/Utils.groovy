#!/usr/bin/groovy
package utilities

import hudson.slaves.EnvironmentVariablesNodeProperty
import hudson.EnvVars

import hudson.model.BuildableItem
import hudson.model.Job
import jenkins.model.*;
import com.cloudbees.plugins.credentials.BaseCredentials



static def loadEnvVariable(String key)
{
  EnvironmentVariablesNodeProperty prop = jenkins.model.Jenkins.instance.getGlobalNodeProperties().get(EnvironmentVariablesNodeProperty.class)
  EnvVars env = prop.getEnvVars()
  def myVariable = env[key]
  return myVariable
}


static BaseCredentials getCredentialsById(String credentialsId) {
    def creds = com.cloudbees.plugins.credentials.CredentialsProvider.lookupCredentials(
             com.cloudbees.plugins.credentials.BaseCredentials.class,
             jenkins.model.Jenkins.instance
             )

     def c = creds.findResult { it.id == credentialsId ? it : null }

     if ( c ) {
         return c
     } else {
         println "could not find credential for ${credentialsId}"
         return null
     }
}
