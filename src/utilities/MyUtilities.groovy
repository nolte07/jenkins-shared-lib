package utilities

import utilities.Utils
import utilities.DescriptionHelper

class MyUtilities {
    static void addMyFeature(def job, def remote_project) {
        job.with {
            description(DescriptionHelper.buildProjectDescription(remote_project))
        }
    }
}