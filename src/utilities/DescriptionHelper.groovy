#!/usr/bin/groovy
package utilities

static def buildProjectGroupDescription(group)
{
    def description = "<h3>${group.name}</h3>"
    description += "<p>${group.description}</p>"
    description += "<div><p><strong>Gitlab Project: </strong><a href=\"${group.getWebUrl()}\">${group.name}</a><p></div>"

    return description
}

static def buildProjectDescription(def project)
{
  def description = "<hr><strong>Project: </strong> <a href=${project.getWebUrl()}>"+project.name+"</a><br>";
  description += "<code>git clone ${project.getSshUrl()}</code><br>"
  description += "<hr>"
  return description;
}