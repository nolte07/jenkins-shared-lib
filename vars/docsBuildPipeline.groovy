#!/usr/bin/groovy

def call(body) {
    node {
        checkout scm
        stage('prepare')
        {
          withPythonEnv('python-auto') {
            pysh 'pip install tox'
          }
        }
        stage('build docs')
        {
          withPythonEnv('python-auto') {
            pysh 'tox -e docs'
          }
          archiveArtifacts('.tox/docs/tmp/html/**/*.*')
        }          
    }
    
}