#!/usr/bin/groovy

def call(folder,project,body) {
    multibranchPipelineJob(folder.name+"/"+project.name){
        body()
    }
}